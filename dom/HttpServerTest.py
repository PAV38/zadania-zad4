from funkload.FunkLoadTestCase import FunkLoadTestCase
import socket, os, unittest, http_server

SERVER_HOST = os.environ.get('http_server', '194.29.175.240:31006')

class HttpServerTest(FunkLoadTestCase):
    def test_dialog(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(("194.29.175.240", 31006))
        for i in range(0,10):
            sock.sendall("")
        sock.close()
if __name__ == '__main__':
    unittest.main()
