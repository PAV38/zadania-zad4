# -*- encoding: utf-8 -*-
import os
import logging
import socket

class aplikacja:

	def http_serve(self, server_socket, html, logger):
		while True:
			# Czekanie na połączenie
			connection, client_address = server_socket.accept()
			try:
				# Odebranie żądania
				request = connection.recv(1024)
				if request:
					print "Odebrano:"
					print request
					address = (request.split("\n")[0])[5:-10]
					print address
					if address=="":
						print os.listdir(".")
						html = "HTTP/1.1 200 OK Content-Type: text/html; charset=UTF-8\r\n\r\n"
						html += "<!DOCTYPE html><html><head></head><body>"
						items = os.listdir(".")
						for item in items:
							print address+item
							if not "." in item:
								item+="/"
							html+= "<a href=\""+item+"\">"+item+"</a><br />"
						html += "</body></html>"
					elif address[-4:]=="html":
						try:
							html = "HTTP/1.1 200 OK Content-Type: text/html; charset=UTF-8\r\n\r\n"
							html+= open(address,"rb").read()
							logger.info("Pobrano strone internetowa. Format: html")
						except:
							html="HTTP/1.0 404 Not Found Content-Type: text/html; charset=UTF-8\r\n\r\n"
							html+="<h1>404 not found</h1>"
							logger.info("Nie znaleziono pliku HTML")
					elif address[-3:]=="txt" or address[-2:]=="py" or address[-3:]=="rst":
						try:
							html = "HTTP/1.1 200 OK Content-Type: text/span; charset=UTF-8\r\n\r\n"
							html += open(address,"rb").read()
							logger.info("Pobrano plik tekstowy. Format: txt")
						except:
							html="HTTP/1.0 404 Not Found Content-Type: text/html; charset=UTF-8\r\n\r\n"
							html+="<h1>404 not found</h1>"
							logger.info("Nie znaleziono pliku txt")
					elif address[-3:]=="png":
						try:
							html = "HTTP/1.0 200 OK Content-Type: image/png;\r\n\r\n"
							html += open(address,"rb").read()
							logger.info("Pobrano obraz. Format: PNG")

						except :
							html="HTTP/1.0 404 Not Found Content-Type: text/html; charset=UTF-8\r\n\r\n"
							html+="<h1>404 not found</h1>"
							logger.info("Nie znaleziono obrazu PNG")
					elif address[-3:] == "jpg":
						try:
							print address
							html = "HTTP/1.1 200 OK Content-Type: image/jpeg; charset=UTF-8\r\n\r\n"
							html+= open(address,"rb").read()
							logger.info("Pobrano obraz. Format: JPEG")
						except:
							html="HTTP/1.0 404 Not Found Content-Type: text/html; charset=UTF-8\r\n\r\n"
							html+="<h1>404 not found</h1>"
							logger.info("Nie znaleziono obrazu JPEG")
					elif not "." in address:
						print address
						try:
							print os.listdir(address)
							html = "HTTP/1.1 200 OK Content-Type: text/html; charset=UTF-8\r\n\r\n"
							html += "<!DOCTYPE html><html><head></head><body>"
							items = os.listdir(address)
							for item in items:
								print address+item
								if "." not in item:
									item+="/"
								html+= "<a href=\""+item+"\">"+item+"</a><br />"
							html += "</body></html>"
						except:
							html="HTTP/1.0 404 Not Found Content-Type: text/html; charset=UTF-8\r\n\r\n"
							html+="<h1>404 not found</h1>"
					else:
						try:
							html = "HTTP/1.1 200 OK Content-Type: plain/text; charset=UTF-8\r\n\r\n"
							html+= open(address).read()
						except:
							html="HTTP/1.0 404 Not Found Content-Type: text/html; charset=UTF-8\r\n\r\n"
							html+="<h1>404 not found</h1>"
							logger.info("Nie wysłano zadnego zapytania do strony")
					connection.sendall(html)
			finally:
				connection.close()


	def __init__(self):
        self.stdin_path = '/dev/null'
        self.stdout_path = '/dev/tty'
        self.stderr_path = '/dev/tty'
        self.pidfile_path = '/tmp/demon.pid'
        self.pidfile_timeout = 5


	def run(self):
        logger = logging.getLogger("DemonLog")
        logger.setLevel(logging.INFO)
        formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
        handler = logging.FileHandler("/tmp/demon.log")
        handler.setFormatter(formatter)
        logger.addHandler(handler)
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        server_address = ('localhost', 31006)
        server_socket.bind(server_address)

        server_socket.listen(10)

        html = ""#open('web/web_page.html').read()

        try:
            self.http_serve(server_socket, html, logger)

        finally:
            server_socket.close()
